#include "ncWindows.h"

WINDOW* winError;
WINDOW* winWaiting;
WINDOW* winTracts;
WINDOW* winMotors;
WINDOW* winCameras;
WINDOW* winBD;
WINDOW* winMode;
WINDOW* ncActiveWindow;

int32_t termHeight;
int32_t termWidth;
int32_t ncTractSelection = 0;
int32_t ncControlSelection = 0;
int32_t winPositionOffsetX = 31;

mutex mu_ncurses;

//UI Strings
static string titleMode = " Sterowanie ";
static string titleTracts = " Trakty ";
static string titleBD = " Bloki Diagnostyczne ";
static string titleStatus = "Status";
static string titleMeasurement = "Pomiar";
static string titleBDID = "BD #";
static string titleMotors = "Napęd";
static string titleCameras = "Kamera";
static string tractNames[NUM_TRACTS + 1] = {"Cyklotron", "Trakt A", "Trakt B", "Trakt C-1", "Trakt C-2", "Trakt C-3", "Trakt D"};

/**************************************** Window Drawing *************************************************/

void NCDrawErrorWindow(string msgError)
{
	lock_guard<mutex> ncguard(mu_ncurses);

	int32_t winErrorWidth =  msgError.length() + 10;

	winError = newwin(winErrorHeight, winErrorWidth, termHeight/2 - winErrorHeight/2, termWidth/2 - winErrorWidth/2);
	wrefresh(winError);
	box(winError, 0, 0);
	wattron(winError, COLOR_PAIR(1));
	mvwprintw(winError, winErrorHeight/2, winErrorWidth/2 - msgError.length()/2, msgError.c_str());
	wattroff(winError, COLOR_PAIR(1));
	wrefresh(winError);
	wgetch(winError);

	//Clear terminal and exit
	cout << "\033[2J\033[1;1H";
	endwin();
}

void NCDrawWaitingMessage(string msgWaiting)
{
	lock_guard<mutex> ncguard(mu_ncurses);

	int32_t winWaitingWidth = msgWaiting.length() + 10;

	winWaiting = newwin(winWaitingHeight, winWaitingWidth, termHeight/2 - winWaitingHeight/2, termWidth/2 - winWaitingWidth/2);
	box(winWaiting, 0, 0);
	mvwprintw(winWaiting, winWaitingHeight/2, winWaitingWidth/2 - msgWaiting.length()/2 + 2, msgWaiting.c_str());
	wrefresh(winWaiting);
}

void NCDrawHelpMenu()
{
	lock_guard<mutex> ncguard(mu_ncurses);

	mvwprintw(stdscr, termHeight - 2, 4, string(60, ' ').c_str());

	attron(A_REVERSE);
	mvwprintw(stdscr, termHeight - 2, 2, "F8");
	attroff(A_REVERSE);

	if (mbGateway->GetUserMode() == Local)
	{
		mvwprintw(stdscr, termHeight - 2, 4, " Sterowanie Zdalne ");

		if (ncActiveWindow != winTracts && ncActiveWindow != nullptr)
		{
			attron(A_REVERSE);
			mvwprintw(stdscr, termHeight - 2, 25, "F9");
			attroff(A_REVERSE);

			mvwprintw(stdscr, termHeight - 2, 27, " Zmiana Traktu ");

			attron(A_REVERSE);
			mvwprintw(stdscr, termHeight - 2, 44, "F12");
			attroff(A_REVERSE);

			mvwprintw(stdscr, termHeight - 2, 47, " Wyjdź ");
		}
		else
		{
			attron(A_REVERSE);
			mvwprintw(stdscr, termHeight - 2, 25, "F12");
			attroff(A_REVERSE);

			mvwprintw(stdscr, termHeight - 2, 28, " Wyjdź ");
		}
	}
	else if (mbGateway->GetUserMode() == Remote || mbGateway->GetUserMode() == Waiting)
	{
		mvwprintw(stdscr, termHeight - 2, 4, " Sterowanie Lokalne ");

		attron(A_REVERSE);
		mvwprintw(stdscr, termHeight - 2, 26, "F12");
		attroff(A_REVERSE);

		mvwprintw(stdscr, termHeight - 2, 29, " Wyjdź ");
	}
}

void NCDrawMainWindows()
{
	{
		lock_guard<mutex> ncguard(mu_ncurses);

		if (mbGateway->GetUserMode() == Local) winPositionOffsetX = 31;
		else if (mbGateway->GetUserMode() == Remote) winPositionOffsetX = 22;

		//Create tract selection window
		winTracts = newwin(winTractsHeight, winTractsWidth, termHeight/2 - winBDHeight/2, termWidth/2 - winTractsWidth - winPositionOffsetX);
		box(winTracts, 0, 0);
		mvwprintw(winTracts, 0, winTractsWidth/2 - titleTracts.length()/2, titleTracts.c_str());
		wrefresh(winTracts);

		//Create user mode window
		winMode = newwin(winModeHeight, winModeWidth, termHeight/2 - winBDHeight/2 + winTractsHeight + 1,  termWidth/2 - winModeWidth - winPositionOffsetX);
		box(winMode, 0, 0);
		mvwprintw(winMode, 0, winModeWidth/2 - titleMode.length()/2, titleMode.c_str());
		wrefresh(winMode);

		//Store initial window selection
		ncActiveWindow = winTracts;
	}

	NCUpdateWinTracts();
	NCUpdateWinUserMode();
}

void NCDrawUserControlWindows()
{
	{
		lock_guard<mutex> ncguard(mu_ncurses);

		if (mbGateway->GetUserMode() == Local)
		{
			//Create motor control window
			winMotors = newwin(winMotorsHeight, winMotorsWidth, termHeight/2 - winBDHeight/2, getbegx(winTracts) + winTractsWidth + 3);
			wborder(winMotors, 0, ' ', 0, 0, 0, ACS_HLINE, 0, ACS_HLINE);

			winCameras = newwin(winCamerasHeight, winCamerasWidth, termHeight/2 - winBDHeight/2, getbegx(winTracts) + winTractsWidth + winMotorsWidth + winBDWidthLocal + 3);
			wborder(winCameras, ' ', 0, 0, 0, ACS_HLINE, 0, ACS_HLINE, 0);
		}
		else if (mbGateway->GetUserMode() == Remote)
		{
			winCameras = newwin(winCamerasHeight, winCamerasWidth, termHeight/2 - winBDHeight/2, getbegx(winTracts) + winTractsWidth + winBDWidthRemote + 3);
			wborder(winCameras, ' ', 0, 0, 0, ACS_HLINE, 0, ACS_HLINE, 0);
		}

		//Print user subtitles and refresh
		if (mbGateway->GetActiveTract() != None) NCPrintSubtitlesUser();
		if (mbGateway->GetUserMode() == Local) wrefresh(winMotors);
		if (mbGateway->GetUserMode() == Local || mbGateway->GetUserMode() == Remote) wrefresh(winCameras);

		ncActiveWindow = winTracts;
	}

	if (mbGateway->GetUserMode() == Local) NCClearWinMotors();
	if (mbGateway->GetUserMode() == Local || mbGateway->GetUserMode() == Remote) NCClearWinCameras();
	mbGateway->SetInitializeRTU(); //windows updated in RTU thread
}

void NCDrawBDWindow()
{
	lock_guard<mutex> ncguard(mu_ncurses);

	if (winBD != NULL)
	{
		wclear(winBD);
		wrefresh(winBD);
		delwin(winBD);
	}

	if (mbGateway != nullptr)
	{
		//Re-draw diagnostic blocks window according to active user mode
		if (mbGateway->GetUserMode() == Local)
		{
			winBD = newwin(winBDHeight, winBDWidthLocal, termHeight/2 - winBDHeight/2, getbegx(winTracts) +  winTractsWidth + winMotorsWidth + 3);
			wrefresh(winBD);
			wborder(winBD, ' ', ' ', 0, 0, ACS_HLINE, ACS_HLINE, ACS_HLINE, ACS_HLINE);
			mvwprintw(winBD, 0, 5, titleBD.c_str());
			if (mbGateway->GetActiveTract() != None) NCPrintSubtitlesBD();  //print diagnostic blocks subtitles
			else mvwprintw(winBD, winBDHeight/2, winBDWidthLocal/2 - 6, "Brak");
			wrefresh(winBD);
		}
		else if (mbGateway->GetUserMode() == Remote)
		{
			winBD = newwin(winBDHeight, winBDWidthRemote, termHeight/2 - winBDHeight/2, getbegx(winTracts) + winTractsWidth + 3);
			wborder(winBD, 0, ' ', 0, 0, 0, ACS_HLINE, 0, ACS_HLINE);;
			mvwprintw(winBD, 0, winBDWidthRemote/2 - 1, titleBD.c_str());
			if (mbGateway->GetActiveTract() != None) NCPrintSubtitlesBD();  //print diagnostic blocks subtitles
			else mvwprintw(winBD, winBDHeight/2, winBDWidthRemote/2 + 6, "Brak");
			wrefresh(winBD);
		}
	}
	else
	{
		//Called before Modbus gateway has been initialized
		winBD = newwin(winBDHeight, winBDWidthRemote, termHeight/2 - winBDHeight/2, getbegx(winTracts) + winTractsWidth + 3);
		box(winBD, 0, 0);
		mvwprintw(winBD, 0, winBDWidthRemote/2 - titleBD.length()/2, titleBD.c_str());
		mvwprintw(winBD, winBDHeight/2, winBDWidthRemote/2 - 6, "Brak");
		wrefresh(winBD);
	}
}


/************************************* Window Deletion ***************************************************/

void NCDeleteWaitingMessage()
{
	lock_guard<mutex> ncguard(mu_ncurses);

	if (winWaiting != nullptr)
	{
		wclear(winWaiting);
		delwin(winWaiting);
	}
}

void NCDeleteMainWindows()
{
	lock_guard<mutex> ncguard(mu_ncurses);

	if (winTracts != nullptr)
	{
		wclear(winTracts);
		delwin(winTracts);
		winTracts = nullptr;
	}

	if (winMode != nullptr)
	{
		wclear(winMode);
		delwin(winMode);
		winMode = nullptr;
	}
}

void NCDeleteUserWindows()
{
	lock_guard<mutex> ncguard(mu_ncurses);

	if (winMotors != nullptr)
	{
		wclear(winMotors);
		delwin(winMotors);
		winMotors = nullptr;
	}

	if (winCameras != nullptr)
	{
		wclear(winCameras);
		delwin(winCameras);
		winCameras = nullptr;
	}
}

void NCDeleteBDWindow(void)
{
	if (winBD != nullptr)
	{
		wclear(winBD);
		delwin(winBD);
		winBD = nullptr;
	}
}

/************************************* Window Subtitles **************************************************/

void NCPrintSubtitlesBD()
{
	if (mbGateway != nullptr)
	{
		if (mbGateway->GetUserMode() == Local)
		{
			//Print sub-titles and refresh
			wattron(winBD, A_UNDERLINE);
			mvwprintw(winBD, 3, winBDWidthLocal/2 - titleStatus.length()/2 - 9, titleStatus.c_str());
			mvwprintw(winBD, 3, winBDWidthLocal/2 - titleMeasurement.length()/2 + 8, titleMeasurement.c_str());
			wattroff(winBD, A_UNDERLINE);
		}
		else if (mbGateway->GetUserMode() == Remote)
		{
			wattron(winBD, A_UNDERLINE);
			mvwprintw(winBD, 3, winBDWidthLocal/2 - titleStatus.length()/2 - 9, titleBDID.c_str());
			mvwprintw(winBD, 3, winBDWidthLocal/2 - titleStatus.length()/2 + 3, titleStatus.c_str());
			mvwprintw(winBD, 3, winBDWidthLocal/2 - titleMeasurement.length()/2 + 18, titleMeasurement.c_str());
			wattroff(winBD, A_UNDERLINE);
		}
	}
	else
	{
		wattron(winBD, A_UNDERLINE);
		mvwprintw(winBD, 3, winBDWidthLocal/2 - titleStatus.length()/2 - 9, titleBDID.c_str());
		mvwprintw(winBD, 3, winBDWidthLocal/2 - titleStatus.length()/2 + 3, titleStatus.c_str());
		mvwprintw(winBD, 3, winBDWidthLocal/2 - titleMeasurement.length()/2 + 18, titleMeasurement.c_str());
		wattroff(winBD, A_UNDERLINE);
	}
}

void NCPrintSubtitlesUser()
{
	if (mbGateway->GetUserMode() == Local)
	{
		wattron(winMotors, A_UNDERLINE);
		mvwprintw(winMotors, 3, winMotorsWidth/2 - titleMotors.length()/2 + 9, titleMotors.c_str());
		mvwprintw(winMotors, 3, winMotorsWidth/2 - titleBDID.length()/2 - 3, titleBDID.c_str());
		wattroff(winMotors, A_UNDERLINE);
	}

	wattron(winCameras, A_UNDERLINE);
	mvwprintw(winCameras, 3, winCamerasWidth/2 - titleCameras.length()/2 - 3, titleCameras.c_str());
	wattroff(winCameras, A_UNDERLINE);
}

/************************************* Window Clearing ***************************************************/

void NCClearWinMain()
{
	lock_guard<mutex> ncguard(mu_ncurses);

	for (uint8_t i = termHeight / 2 - winBDHeight / 2; i <= termHeight / 2 + winBDHeight / 2; i++)
	{
		mvprintw(i, 1, string(termWidth - 2, ' ').c_str());
	}

	refresh();
}

void NCClearWinTracts()
{
	lock_guard<mutex> ncguard(mu_ncurses);

	for (uint8_t i = 3; i < winTractsHeight - 1; i++)
	{
		mvwprintw(winTracts, i, 1, string(winTractsWidth - 2, ' ').c_str());
	}

	wrefresh(winTracts);
}

void NCClearWinUserMode()
{
	lock_guard<mutex> ncguard(mu_ncurses);

	for (uint8_t i = 3; i < winModeHeight - 1; i++)
	{
		mvwprintw(winMode, i, 1, string(winModeWidth - 2, ' ').c_str());
	}

	wrefresh(winMode);
}

void NCClearWinBD()
{
	lock_guard<mutex> ncguard(mu_ncurses);

	//Clear window
	for (uint8_t i = 4; i < winBDHeight - 1; i++)
	{
		if (mbGateway->GetUserMode() == Local) mvwprintw(winBD, i, 1, string(winBDWidthLocal - 2, ' ').c_str());
		else if (mbGateway->GetUserMode() == Remote) mvwprintw(winBD, i, 1, string(winBDWidthRemote - 2, ' ').c_str());
	}

	//Print subtitles or no blocks selected message
	if (mbGateway->GetActiveTract() == None)
	{
		mvwprintw(winBD, 3, 1, string(winBDWidthRemote - 2, ' ').c_str());
		if (mbGateway->GetUserMode() == Local) mvwprintw(winBD, winBDHeight/2, winBDWidthLocal/2 - 6, "Brak");
		else if (mbGateway->GetUserMode() == Remote) mvwprintw(winBD, winBDHeight/2, winBDWidthRemote/2 + 6, "Brak");
	}
	else
	{
		NCPrintSubtitlesBD(); //print subtitles
	}

	wrefresh(winBD);
}

void NCClearWinMotors()
{
	lock_guard<mutex> ncguard(mu_ncurses);

	for (uint8_t i = 3; i < winMotorsHeight - 1; i++)
	{
		mvwprintw(winMotors, i, 1, string(winMotorsWidth - 1, ' ').c_str());
	}

	if (mbGateway->GetActiveTract() != None) NCPrintSubtitlesUser(); //print user subtitles
	wrefresh(winMotors);
}


void NCClearWinCameras()
{
	lock_guard<mutex> ncguard(mu_ncurses);

	for (uint8_t i = 3; i < winCamerasHeight - 1; i++)
	{
		mvwprintw(winCameras, i, 0, string(winCamerasWidth - 1, ' ').c_str());
	}

	if (mbGateway->GetActiveTract() != None) NCPrintSubtitlesUser(); //print user subtitles
	wrefresh(winCameras);
}


/************************************* Window Updating/Printing ******************************************/

void NCUpdateWinTracts()
{
	eTract eActiveTract = mbGateway->GetActiveTract();
	eMode eUserMode = mbGateway->GetUserMode();

	lock_guard<mutex> ncguard(mu_ncurses);

	for (int32_t i = 0; i < NUM_TRACTS + 1; i++)
	{
		if (i == ncTractSelection && eUserMode == Local && ncActiveWindow == winTracts) wattron(winTracts, A_REVERSE);
		if (i == eActiveTract) wattron(winTracts, A_UNDERLINE);
		mvwprintw(winTracts, i*2 + 3, winTractsWidth/2 - tractNames[i].length()/2, tractNames[i].c_str());
		wattroff(winTracts, A_REVERSE);
		wattroff(winTracts, A_UNDERLINE);
	}

	wrefresh(winTracts);
}

void NCUpdateWinUserMode()
{
	eMode eUserMode = mbGateway->GetUserMode();

	{
		lock_guard<mutex> ncguard(mu_ncurses);

		mvwprintw(winMode, winModeHeight/2 , winModeWidth/2 - 3, "       ");

		if (eUserMode == Local)
		{
			wattron(winMode, COLOR_PAIR(2));
			mvwprintw(winMode, winModeHeight/2 , winModeWidth/2 - 3, "Lokalne");
			wattroff(winMode, COLOR_PAIR(2));
		}
		else
		{
			wattron(winMode, COLOR_PAIR(3));
			mvwprintw(winMode, winModeHeight/2 , winModeWidth/2 - 3, "Zdalne");
			wattroff(winMode, COLOR_PAIR(3));
		}

		wrefresh(winMode);
	}
}

void NCUpdateWinBD()
{
	uint8_t bdID = 0;
	uint8_t bdStringStartX = 5;
	const uint8_t* activeBD;
	uint8_t numBD;
	string bdStatus;

	activeBD = mbGateway->tract2bd_id[mbGateway->GetActiveTract()];
	numBD = mbGateway->tract2bd_num[mbGateway->GetActiveTract()];

	{
		lock_guard<mutex> ncguard(mu_ncurses);
		lock_guard<mutex> local_guard(mbGateway->mu_local);

		for (uint8_t i = 0; i < numBD; i++)
		{
			bdID = activeBD[i] - 1;

			//Clear line and print BD ID
			if (mbGateway->GetUserMode() == Local)
			{
				mvwprintw(winBD, i*3 + 6, 1, string(winBDWidthLocal - 2,' ').c_str());
			}
			else if (mbGateway->GetUserMode() == Remote)
			{
				mvwprintw(winBD, i*3 + 6, 1, string(winBDWidthRemote - 2,' ').c_str());
				mvwprintw(winBD, i*3 + 6, bdStringStartX + 2, ("BD " + to_string(bdID + 1) + ": ").c_str());
			}

			if (!mbGateway->CheckExcludedBD(bdID))
			{
				///Check for Modbus errors and Motor errors
				if (mbGateway->DiscreteInputs[NUM_BD*NUM_INPUTS_BD + bdID*NUM_ERRORS_LIBMODBUS] ||
						mbGateway->DiscreteInputs[NUM_BD*NUM_INPUTS_BD + bdID*NUM_ERRORS_LIBMODBUS + 1] ||
						mbGateway->DiscreteInputs[NUM_BD*NUM_INPUTS_BD + bdID*NUM_ERRORS_LIBMODBUS + 2])
				{
					bdStatus = "Błąd Modbus";
				}
				else if (mbGateway->DiscreteInputs[bdID*NUM_INPUTS_BD + 4])
				{
					bdStatus = "Napęd Włączony";
				}
				else if (mbGateway->DiscreteInputs[bdID*NUM_INPUTS_BD + 1])
				{
					bdStatus = "Błąd Napędu";
				}
				else if (mbGateway->DiscreteInputs[bdID*NUM_INPUTS_BD])
				{
					bdStatus = " Wysunięty";
				}
				else
				{
					bdStatus =  " Schowany";
				}

				//Set text color
				if (bdStatus == "Błąd Napędu")
				{
					wattron(winBD, COLOR_PAIR(3)); //yellow
				}
				else if (bdStatus == "Błąd Modbus")
				{
					wattron(winBD, COLOR_PAIR(1)); //red
				}
				else if (bdStatus == " Schowany" || bdStatus == " Wysunięty")
				{
					wattron(winBD, COLOR_PAIR(2)); //green
				}
				else if (bdStatus == "Napęd Włączony")
				{
					wattron(winBD, COLOR_PAIR(2)); //green
				}
			}

			if (mbGateway->GetUserMode() == Local)
			{
				if (!mbGateway->CheckExcludedBD(bdID))
				{
					if (bdStatus == "Napęd Włączony") mvwprintw(winBD, i * 3 + 6, bdStringStartX - 1, bdStatus.c_str());
					else mvwprintw(winBD, i * 3 + 6, bdStringStartX, bdStatus.c_str());
					wattroff(winBD, COLOR_PAIR(1));
					wattroff(winBD, COLOR_PAIR(2));
					wattroff(winBD, COLOR_PAIR(3));

					if (bdStatus == " Wysunięty")
					{
						//Print diagnostic block reading
						mvwprintw(winBD, i * 3 + 6, winBDWidthLocal/2 - titleMeasurement.length()/2 + 8,
								(to_string((uint16_t)(mbGateway->InputRegisters[bdID]*ADC_SCALING + 0.5f)) + " nA").c_str());
					}
					else
					{
						//Print diagnostic block reading
						mvwprintw(winBD, i * 3 + 6, winBDWidthLocal/2 - titleMeasurement.length()/2 + 8, "  -");
					}
				}
				else
				{
					mvwprintw(winBD, i * 3 + 6, bdStringStartX + 4, string(1,'-').c_str());
					mvwprintw(winBD, i * 3 + 6, bdStringStartX + 21, string(1,'-').c_str());
				}
			}
			else if (mbGateway->GetUserMode() == Remote)
			{
				if (!mbGateway->CheckExcludedBD(bdID))
				{
					if (bdStatus == "Napęd Włączony") mvwprintw(winBD, i * 3 + 6, bdStringStartX + 11, bdStatus.c_str());
					else mvwprintw(winBD, i * 3 + 6, bdStringStartX + 12, bdStatus.c_str());
					wattroff(winBD, COLOR_PAIR(1));
					wattroff(winBD, COLOR_PAIR(2));
					wattroff(winBD, COLOR_PAIR(3));

					if (bdStatus == " Wysunięty")
					{
						//Print diagnostic block reading
						mvwprintw(winBD, i * 3 + 6, winBDWidthLocal/2 - titleMeasurement.length()/2 + 18,
								(to_string((uint16_t)(mbGateway->InputRegisters[bdID]*ADC_SCALING + 0.5f)) + " nA").c_str());
					}
					else
					{
						//Print diagnostic block reading
						mvwprintw(winBD, i * 3 + 6, winBDWidthLocal/2 - titleMeasurement.length()/2 + 18, "  -");
					}
				}
				else
				{
					mvwprintw(winBD, i * 3 + 6, bdStringStartX + 16, string(1,'-').c_str());
					mvwprintw(winBD, i * 3 + 6, bdStringStartX + 31, string(1,'-').c_str());
				}
			}

			wrefresh(winBD);
		}
	}
}

void NCUpdateWinMotors()
{
	eTract eActiveTract = mbGateway->GetActiveTract();

	if (eActiveTract != None)
	{
		uint8_t bdID;
		const uint8_t* activeBD;
		uint8_t numBD;
		uint8_t bdStringStartX;

		activeBD = mbGateway->tract2bd_id[eActiveTract];
		numBD = mbGateway->tract2bd_num[eActiveTract];

		lock_guard<mutex> ncguard(mu_ncurses);

		bdStringStartX = 7;

		for (uint8_t i = 0; i < numBD; i++)
		{
			bdID = activeBD[i] - 1;

			//Clear line and print BD ID
			mvwprintw(winMotors, i*3 + 6, 1, string(winMotorsWidth - 1,' ').c_str());
			mvwprintw(winMotors, i*3 + 6, bdStringStartX, ("BD " + to_string(bdID + 1) + ": ").c_str());

			//Print button
			if (ncActiveWindow == winMotors && ncControlSelection == i) wattron(winMotors, A_REVERSE);

			if (!mbGateway->CheckExcludedBD(bdID))
			{
				if (mbGateway->DiscreteInputs[NUM_BD * NUM_INPUTS_BD + bdID * NUM_ERRORS_LIBMODBUS] ||     //modbus error
					mbGateway->DiscreteInputs[NUM_BD * NUM_INPUTS_BD + bdID * NUM_ERRORS_LIBMODBUS + 1] || //modbus error
					mbGateway->DiscreteInputs[NUM_BD * NUM_INPUTS_BD + bdID * NUM_ERRORS_LIBMODBUS + 2])   //modbus error
				{
					//Display button according to last known motor position
					if (mbGateway->DiscreteInputs[bdID * NUM_INPUTS_BD]) mvwprintw(winMotors, i*3 + 6, bdStringStartX + 10, "Schowaj"); //motor extended
					else mvwprintw(winMotors, i*3 + 6,  bdStringStartX + 11, "Wysuń"); //motor hidden
				}
				else if (mbGateway->DiscreteInputs[bdID * NUM_INPUTS_BD + 4]) //motor active
				{
					wattroff(winMotors, A_REVERSE);
					mvwprintw(winMotors, i*3 + 6,  bdStringStartX + 13, "-");
				}
				else if (mbGateway->DiscreteInputs[bdID * NUM_INPUTS_BD + 1]) //motor error
				{
					//Display button according to last known motor position
					if (mbGateway->DiscreteInputs[bdID * NUM_INPUTS_BD]) mvwprintw(winMotors, i*3 + 6, bdStringStartX + 10, "Schowaj"); //motor extended
					else mvwprintw(winMotors, i*3 + 6,  bdStringStartX + 11, "Wysuń"); //motor hidden
				}
				else if (!mbGateway->DiscreteInputs[bdID * NUM_INPUTS_BD + 2]) mvwprintw(winMotors, i*3 + 6,  bdStringStartX + 11, "Wysuń"); //motor retreacted
				else if (!mbGateway->DiscreteInputs[bdID * NUM_INPUTS_BD + 3]) mvwprintw(winMotors, i*3 + 6, bdStringStartX + 10, "Schowaj"); //motor extended
			}
			else
			{
				mvwprintw(winMotors, i*3 + 6,  bdStringStartX + 13, string(1,'-').c_str());
			}

			wattroff(winMotors, A_REVERSE);
		}

		wrefresh(winMotors);
	}
}

void NCUpdateWinCameras()
{
	eTract eActiveTract = mbGateway->GetActiveTract();

	if (eActiveTract != None)
	{
		uint8_t bdID;
		const uint8_t* activeBD;
		uint8_t numBD;
		uint16_t bdCurrentCamera = 0;

		activeBD = mbGateway->tract2bd_id[eActiveTract];
		numBD = mbGateway->tract2bd_num[eActiveTract];

		{
			lock_guard<mutex> guard_state(mbGateway->mu_state);
			bdCurrentCamera = mbGateway->bdCurrentCamera; //get active camera
		}

		lock_guard<mutex> ncguard(mu_ncurses);

		for (uint8_t i = 0; i < numBD; i++)
		{
			bdID = activeBD[i] - 1;

			//Clear line
			mvwprintw(winCameras, i*3 + 6, 0, string(winCamerasWidth - 2,' ').c_str());

			if (!mbGateway->CheckExcludedBD(bdID))
			{
				if (mbGateway->DiscreteInputs[NUM_BD*NUM_INPUTS_BD + NUM_BD*NUM_ERRORS_LIBMODBUS])
				{
					if (ncActiveWindow == winCameras && ncControlSelection == i) wattron(winCameras, COLOR_PAIR(4));
					else wattron(winCameras, COLOR_PAIR(1)); //red

					mvwprintw(winCameras, i * 3 + 6, winCamerasWidth / 2 - 8, "Błąd Modbus");
					wattroff(winCameras, COLOR_PAIR(1));
					wattroff(winCameras, COLOR_PAIR(4));
				}
				else if (bdCurrentCamera == activeBD[i])
				{
					if (ncActiveWindow == winCameras && ncControlSelection == i) wattron(winCameras, COLOR_PAIR(4));
					else wattron(winCameras, COLOR_PAIR(2)); //green
					mvwprintw(winCameras, i * 3 + 6, winCamerasWidth / 2 - 7, "Aktywna");
					wattroff(winCameras, COLOR_PAIR(2));
					wattroff(winCameras, COLOR_PAIR(4));
				}
				else
				{
					if (ncActiveWindow == winCameras && ncControlSelection == i) wattron(winCameras, COLOR_PAIR(4));
					else wattron(winCameras, COLOR_PAIR(3)); //yellow
					mvwprintw(winCameras, i*3 + 6, winCamerasWidth/2 - 7, "Wyłączona");
					wattroff(winCameras, COLOR_PAIR(3));
					wattroff(winCameras, COLOR_PAIR(4));
				}
			}
			else
			{
				if (ncActiveWindow == winCameras && ncControlSelection == i) wattron(winCameras, COLOR_PAIR(4));
				mvwprintw(winCameras, i * 3 + 6, winCamerasWidth/2 - 4, string(1,'-').c_str());
				wattroff(winCameras, COLOR_PAIR(4));
			}

			wattroff(winCameras, A_REVERSE);
		}

		wrefresh(winCameras);
	}
}
