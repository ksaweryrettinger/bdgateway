#ifndef NCWINDOWS_H_
#define NCWINDOWS_H_

#include <cstdint>
#include <thread>
#include <mutex>
#include <iostream>
#include "ncurses.h"
#include "cModbusGateway.h"
#include "params.h"

using namespace std;

//Globals
extern WINDOW* winError;
extern WINDOW* winWaiting;
extern WINDOW* winTracts;
extern WINDOW* winMotors;
extern WINDOW* winCameras;
extern WINDOW* winBD;
extern WINDOW* winMode;
extern WINDOW* ncActiveWindow;

extern int32_t termHeight;
extern int32_t termWidth;
extern int32_t ncTractSelection;
extern int32_t ncControlSelection;

extern mutex mu_ncurses;

extern int32_t winPositionOffsetX;
const int32_t winErrorHeight = 7;
const int32_t winWaitingHeight = 7;
const int32_t winTractsHeight = 19;
const int32_t winTractsWidth = 22;
const int32_t winMotorsHeight = 29;
const int32_t winMotorsWidth = 25;
const int32_t winCamerasHeight = 29;
const int32_t winCamerasWidth = 17;
const int32_t winBDHeight = 29;
const int32_t winBDWidthLocal = 39;
const int32_t winBDWidthRemote = 48;
const int32_t winModeHeight = 5;
const int32_t winModeWidth = 22;

//Drawing of windows
void NCDrawErrorWindow(string);
void NCDrawWaitingMessage(string);
void NCDrawMainWindows(void);
void NCDrawUserControlWindows(void);
void NCDrawBDWindow(void);

//Window deletion
void NCDeleteWaitingMessage(void);
void NCDeleteMainWindows(void);
void NCDeleteUserWindows(void);
void NCDeleteBDWindow(void);

//Printing window subtitles
void NCPrintSubtitlesBD(void);
void NCPrintSubtitlesUser(void);

//Clearing of windows
void NCClearWinMain(void);
void NCClearWinUserMode(void);
void NCClearWinTracts(void);
void NCClearWinBD(void);
void NCClearWinMotors(void);
void NCClearWinCameras(void);

//Updating windows
void NCDrawHelpMenu(void);
void NCUpdateWinUserMode(void);
void NCUpdateWinTracts(void);
void NCUpdateWinBD(void);
void NCUpdateWinMotors(void);
void NCUpdateWinCameras(void);

#endif
