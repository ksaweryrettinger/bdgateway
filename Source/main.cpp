/****************************************************************************
/ Name        : BDGateway.cpp
/ Author      : Ksawery Wieczorkowski-Rettinger
/ Version     : 1.0
/ Copyright   : ŚLCJ Uniwersytet Warszawski
/ Description : BD Gateway
*****************************************************************************/

#include <sys/file.h>
#include <errno.h>
#include <iostream>
#include <clocale>
#include <cstdint>
#include <mutex>
#include "ncurses.h"
#include "ncWindows.h"
#include "cModbusGateway.h"
#include "params.h"

using namespace std;
cModbusGateway* mbGateway;

int main(int argc, char** argv)
{
	uint8_t bdID;
	int32_t keypress;
	int32_t numActiveBD = 0;
	bool bCloseApplication = false;
	const uint8_t* activeBD;

	//Add support for polish characters
	setlocale(LC_ALL, "pl_PL.utf8");

	//ncurses intialisation
	initscr();
	cbreak(); //enable exit program using ctrl+c
	noecho(); //don't echo user inputs to screen
	curs_set(0);

	//Setup colors
	start_color();
	use_default_colors();
	init_color(COLOR_YELLOW, 900, 900, 0);
	init_color(COLOR_WHITE, 980, 980, 980);
	init_pair(1, COLOR_RED, -1); //red foreground
	init_pair(2, COLOR_GREEN, -1); //green foreground
	init_pair(3, COLOR_YELLOW, -1); //yellow foreground
	init_pair(4, COLOR_BLACK, COLOR_WHITE); //white foreground

	//Get terminal size
	getmaxyx(stdscr, termHeight, termWidth);

	//Add main title
	attron(A_BOLD);
	string titleMain = "Bloki Diagnostyczne - Modbus Gateway";
	mvwprintw(stdscr, 3, termWidth/2 - titleMain.length()/2 + 1, titleMain.c_str());
	attroff(A_BOLD);
	refresh();

	//Ensure only one instance of the application is running
	int32_t pid_file = open("/tmp/bdgateway.pid", O_CREAT | O_RDWR, 0666);
	int32_t rc = flock(pid_file, LOCK_EX | LOCK_NB);

	if (rc)
	{
	    if (EWOULDBLOCK == errno)
	    {
	    	wattron(winError, COLOR_PAIR(1));
	    	NCDrawErrorWindow("  Proces już istnieje!");
	    	return 0;
	    }
	}

	//Create and initialize Modbus gateway
	mbGateway = new cModbusGateway();

	//Initialize gateway and check for errors
	if (mbGateway != nullptr)
	{
		if (!mbGateway->Init()) NCDrawErrorWindow("    Błąd Zbioru Konfiguracyjnego!");
	}
	else NCDrawErrorWindow("    Błąd Zbioru Konfiguracyjnego!");

	NCDrawWaitingMessage("Oczekiwanie Na Wiadomość Ze Sterowni ");
	NCDrawHelpMenu();

	keypad(stdscr, true);

	//Main program loop
	while (!bCloseApplication)
	{
		keypress = getch();

		//User control only in LOCAL MODE
		if (mbGateway->GetUserMode() == Local)
		{
			switch (keypress)
			{
			case KEY_UP:

				if (ncActiveWindow == winTracts)
				{
					ncTractSelection--;
					if (ncTractSelection < 0) ncTractSelection = 0;
					NCUpdateWinTracts();
				}
				else if (ncActiveWindow == winMotors || ncActiveWindow == winCameras)
				{
					//Select next active diagnostic block (ignore deactivated blocks)
					if (ncControlSelection > 0)
					{
						activeBD = mbGateway->tract2bd_id[mbGateway->GetActiveTract()];
						bdID = activeBD[ncControlSelection - 1];

						if (mbGateway->CheckExcludedBD(bdID - 1))
						{
							for (int8_t i = ncControlSelection - 2; i >= 0; i--)
							{
								if (!mbGateway->CheckExcludedBD(activeBD[i] - 1))
								{
									ncControlSelection = i;
									break;
								}
							}
						}
						else
						{
							ncControlSelection--;
						}
					}

					if (ncControlSelection < 0) ncControlSelection = 0;
					if (ncActiveWindow == winMotors && mbGateway->IsInitializedRTU()) NCUpdateWinMotors();
					else if (ncActiveWindow == winCameras && mbGateway->IsInitializedRTU()) NCUpdateWinCameras();
				}

				break;

			case KEY_DOWN:

				if (ncActiveWindow == winTracts)
				{
					ncTractSelection++;
					if (ncTractSelection >= NUM_TRACTS + 1) ncTractSelection = NUM_TRACTS;
					NCUpdateWinTracts();
				}
				else if (ncActiveWindow == winMotors || ncActiveWindow == winCameras)
				{
					//Select next active diagnostic block (ignore deactivated blocks)
					if (ncControlSelection < (numActiveBD - 1))
					{
						activeBD = mbGateway->tract2bd_id[mbGateway->GetActiveTract()];
						bdID = activeBD[ncControlSelection + 1];

						if (mbGateway->CheckExcludedBD(bdID - 1))
						{
							for (uint8_t i = ncControlSelection + 2; i < numActiveBD; i++)
							{
								if (!mbGateway->CheckExcludedBD(activeBD[i] - 1))
								{
									ncControlSelection = i;
									break;
								}
							}
						}
						else
						{
							ncControlSelection++;
						}
					}

					if (ncControlSelection >= numActiveBD) ncControlSelection = numActiveBD - 1;
					if (ncActiveWindow == winMotors && mbGateway->IsInitializedRTU()) NCUpdateWinMotors();
					else if (ncActiveWindow == winCameras && mbGateway->IsInitializedRTU()) NCUpdateWinCameras();
				}

				break;

			case KEY_RIGHT:

				if (ncActiveWindow == winMotors && (mbGateway->GetActiveTract() != None) && mbGateway->IsInitializedRTU())
				{
					ncActiveWindow = winCameras;
					NCUpdateWinMotors();
					NCUpdateWinCameras();
				}

				break;

			case KEY_LEFT:

				if (ncActiveWindow == winCameras && mbGateway->IsInitializedRTU())
				{
					ncActiveWindow = winMotors;
					NCUpdateWinCameras();
					NCUpdateWinMotors();
				}

				break;

			case KEY_F(9): //TRACT SELECTION

				if ((ncActiveWindow == winMotors || ncActiveWindow == winCameras) && mbGateway->IsInitializedRTU())
				{
					ncActiveWindow = winTracts;
					NCUpdateWinMotors();
					NCUpdateWinCameras();
					NCUpdateWinTracts();
					NCDrawHelpMenu();
				}

				break;

			case 10:
			case (KEY_ENTER): //RETURN KEY

				if (ncActiveWindow == winTracts) //TRACTS WINDOW
				{
					ncControlSelection = 0;

					if (ncTractSelection > 0)
					{
						ncActiveWindow = winMotors;
						numActiveBD = mbGateway->tract2bd_num[(eTract) ncTractSelection];
					}
					else numActiveBD = 0;

					//Select next active diagnostic block (ignore deactivated blocks)
					if (ncControlSelection < numActiveBD)
					{
						activeBD = mbGateway->tract2bd_id[(eTract)ncTractSelection];
						bdID = activeBD[ncControlSelection];

						if (mbGateway->CheckExcludedBD(bdID - 1))
						{
							for (uint8_t i = ncControlSelection + 1; i < numActiveBD; i++)
							{
								if (!mbGateway->CheckExcludedBD(activeBD[i] - 1))
								{
									ncControlSelection = i;
									break;
								}
								else if (i == (numActiveBD - 1))
								{
									ncActiveWindow = winTracts;
								}
							}
						}
					}

					mbGateway->SetActiveTract((eTract)ncTractSelection);

					NCDrawHelpMenu();
				}
				else if (ncActiveWindow == winMotors && mbGateway->IsInitializedRTU()) //MOTORS WINDOW
				{
					activeBD = mbGateway->tract2bd_id[mbGateway->GetActiveTract()];
					bdID = activeBD[ncControlSelection];
					if (!mbGateway->CheckExcludedBD(bdID - 1)) mbGateway->SetMotorCommand(bdID);
				}
				else if (ncActiveWindow == winCameras && mbGateway->IsInitializedRTU()) //CAMERAS WINDOW
				{
					activeBD = mbGateway->tract2bd_id[mbGateway->GetActiveTract()];
					bdID = activeBD[ncControlSelection];
					if (!mbGateway->CheckExcludedBD(bdID - 1)) mbGateway->SetActiveCamera(bdID);
				}

				break;
			}
		}

		switch (keypress)
		{
		case KEY_F(8):

			if (mbGateway->GetUserMode() == Waiting) // WAITING -> LOCAL
			{
				mbGateway->SetUserMode(Local);
				ncTractSelection = (int32_t)mbGateway->GetActiveTract();

				//Delete message
				NCDeleteWaitingMessage();
				NCClearWinMain();

				//Draw main and user windows (updated in RTU thread)
				NCDrawMainWindows();
				NCDrawBDWindow();
				NCDrawUserControlWindows();
			}
			else if (mbGateway->GetUserMode() == Local) // LOCAL -> WAITING
			{
				mbGateway->SetUserMode(Waiting);

				//Delete main and user windows
				NCDeleteMainWindows();
				NCDeleteBDWindow();
				NCDeleteUserWindows();
				NCClearWinMain();

				//Display message
				NCDrawWaitingMessage("Oczekiwanie Na Potwierdzenie Ze Sterowni   ");
			}
			else if (mbGateway->GetUserMode() == Remote) // REMOTE -> LOCAL
			{
				mbGateway->SetUserMode(Local);

				//Get latest tract selection
				ncTractSelection = (int32_t)mbGateway->GetActiveTract();

				//Re-draw main and user windows (updated in RTU thread)
				NCDeleteMainWindows();
				NCDeleteUserWindows();
				NCClearWinMain();
				NCDrawMainWindows();
				NCDrawBDWindow();
				NCDrawUserControlWindows();

				//Update main windows
				NCUpdateWinUserMode();
				NCUpdateWinTracts();
			}

			//Update help menu at bottom of screen
			NCDrawHelpMenu();

			break;

		case KEY_F(12): //TERMINATE APP

			bCloseApplication = true;
			break;

		case KEY_F(2): //DEBUGGING REMOTE MODE

			if (mbGateway->GetUserMode() == Waiting)
			{
				mbGateway->SetUserMode(Remote);
				NCDeleteWaitingMessage();
				NCClearWinMain();
				NCDrawMainWindows();
				NCDrawBDWindow();
				NCDrawUserControlWindows();
			}
			break;
		}
	}

	//Release resources and close ncurses
	delete (mbGateway);
	lock_guard<mutex> ncguard(mu_ncurses);
	endwin();

	//Clear terminal
	cout << "\033[2J\033[1;1H";

	return 0;
}
