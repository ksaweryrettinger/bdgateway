#include "cModbusGateway.h"

cModbusGateway::cModbusGateway() : threadTCP(), threadRTU(), threadCameras() //default thread constructors are called
{
	//Initialize user parameters
	eActiveTract = None; 				 //default, no eActiveTract select
	eUserMode = Waiting;				 //default, awaiting remote command
	bdNewCamera = 0;					 //default, no camera selected
	bdCurrentCamera = 0;				 //default, no camera selected

	//Initialize variables and buffers
	fill_n(mbAddress, NUM_BD, 0);
	fill_n(isExcludedBD, NUM_BD, false);
	fill_n(bDisconnected, NUM_BD, false);

	//Modbus mapping parameters
	NumCoils = NUM_BD;
	NumDiscreteInputs = NUM_BD*NUM_INPUTS_BD + NUM_ERRORS_LIBMODBUS*NUM_BD + NUM_BD + 1;
	NumInputRegisters = NUM_BD;
	NumHoldingRegisters = 3;

	//Other variables and pointers
	ct = -1;
	socket = -1;
	mbErrorStatus = 0;
	bdMotorStatus = 0;
	bdMotorSetting = 0;
	tempRegister = 0;
	mbCameraAddress = 0;
	mbCameraError = -1;
	ct_camera = -1;
	ctx_camera = nullptr;
	bTerminateTCP = false;
	bTerminateRTU = false;
	bTerminateCameras = false;
	bTCPActive = false;
	bRTUActive = false;
	bCamerasActive = false;
	bRTUInit = false;
	bUpdateMotors = false;
	bCamerasUpdating = false;
	bCamerasConnectionLost = false;
	mb_mapping = nullptr;
}

bool cModbusGateway::Init()
{
	//Read Modbus RTU configuration file
	if (LoadConfiguration())
	{
		//Create Modbus mapping
		mb_mapping = modbus_mapping_new_start_address(1, NumCoils, 10001, NumDiscreteInputs, 40001, NumHoldingRegisters, 30001, NumInputRegisters);

		//Initialize Modbus RTU read errors
		for (uint8_t i = 0; i < NUM_BD; i++)
		{
			/********************************** Local Data ****************************************/

			{
				lock_guard<mutex> guard_local(mu_local);
				DiscreteInputs[NUM_BD*NUM_INPUTS_BD + i*NUM_ERRORS_LIBMODBUS] = 1;
				DiscreteInputs[NUM_BD*NUM_INPUTS_BD + i*NUM_ERRORS_LIBMODBUS + 1] = 1;
			}

			/********************************** Remote Data ***************************************/

			{
				lock_guard<mutex> guard_remote(mu_remote);
				mb_mapping->tab_input_bits[NUM_BD*NUM_INPUTS_BD + i*NUM_ERRORS_LIBMODBUS] = 1;
				mb_mapping->tab_input_bits[NUM_BD*NUM_INPUTS_BD + i*NUM_ERRORS_LIBMODBUS + 1] = 1;
			}
		}

		/********************************** Remote Data ***************************************/

		{
			lock_guard<mutex> guard_remote(mu_remote);

			//Store excluded diagnostic blocks
			for (uint8_t i = 0; i < NUM_BD; i++) mb_mapping->tab_input_bits[NUM_BD*NUM_INPUTS_BD + NUM_ERRORS_LIBMODBUS*NUM_BD + (i + 1)] = (uint8_t) isExcludedBD[i];
		}

		/********************************** Remote Data ***************************************/

		{
			lock_guard<mutex> guard_remote(mu_remote);
			mb_mapping->tab_registers[2] = (uint16_t) Waiting; //initially in waiting state
		}

		//Create Modbus RTU configuration for camera controller
		ctx_camera = modbus_new_rtu(mbCameraSerialPort.c_str(), MB_BAUD, 'E', 8, 1);
		modbus_set_slave(ctx_camera, mbCameraAddress);
		modbus_rtu_set_serial_mode(ctx_camera, MODBUS_RTU_RS232);
		modbus_set_response_timeout(ctx_camera, 0, MB_RESPONSE_TIMEOUT); //TIMEOUT (100ms Camera RTU Response)
		modbus_set_byte_timeout(ctx_camera, 0, 1000000/MB_15CHAR_TIME); //TIMEOUT (1.5 CHAR TIME)
		ct_camera = modbus_connect(ctx_camera);

		//Read active camera setting from camera controller
		{
			lock_guard<mutex> guard_mbcam(mu_mbcam);
			mbCameraError = modbus_read_registers(ctx_camera, 40001, 1, &tempRegister);
		}

		/********************************** Local Data ****************************************/

		{
			lock_guard<mutex> guard_local(mu_local);
			if (mbCameraError != -1) bdCurrentCamera = tempRegister;
			DiscreteInputs[NUM_BD*NUM_INPUTS_BD + NUM_BD*NUM_ERRORS_LIBMODBUS] = (mbCameraError == -1) ? 1 : 0;
		}

		/********************************** Remote Data ***************************************/

		{
			lock_guard<mutex> guard_remote(mu_remote);
			if (mbCameraError != -1) mb_mapping->tab_registers[1] = tempRegister;
			mb_mapping->tab_input_bits[NUM_BD*NUM_INPUTS_BD + NUM_BD*NUM_ERRORS_LIBMODBUS] = (mbCameraError == -1) ? 1 : 0;
		}

		//Start TCP/IP server
		StartTCP();

		//Start Camera thread
		StartCameras();

		return true;
	}

	return false;
}

bool cModbusGateway::LoadConfiguration()
{
	uint8_t bdNum = 0;
	int32_t bdID = 0;
	string newline;
	ifstream configfile;
    string filedir(string(getenv("HOME")) + "/eclipse-workspace/BDGateway/konfiguracja.txt");
    bool bInvalidAddress = true;
    bool bCameraConfigRead = false;
    bool bCameraConfigOK = false;

    configfile.open(filedir.c_str()); //open configuration file

    if (!configfile.is_open()) return false; //failed to open configuration file

	while (!configfile.eof()) //read configuration file
	{
		getline(configfile, newline);

		if (bdNum == NUM_BD && bCameraConfigOK) break;

		if ((newline[0] != '#') && !(newline.empty()))
		{
			if (newline.substr(0, 2) == "BD")
			{
				bdID = stoi(newline.substr(4, 2)); //diagnostic block ID (1-17)
			}
			else if ((newline.substr(0, 15) == "Kontroler Kamer") && mbCameraSerialPort.empty())
			{
				bCameraConfigRead = true;
			}
			else if (newline.substr(0, 12) == "Adres Modbus") //modbus slave address (1-247)
			{
				if (bCameraConfigRead) //read camera configuration
				{
					if (stoi(newline.substr(14, 3)) >= MB_MIN_ADDRESS && stoi(newline.substr(14, 3)) <= MB_MAX_ADDRESS)
					{
						mbCameraAddress = stoi(newline.substr(14, 3));
						bInvalidAddress = false;
					}
					else bInvalidAddress = true; //invalid Modbus address
				}
				else //read diagnostic block configuration
				{
					if (bdID > 0 && bdID <= NUM_BD && mbAddress[bdID - 1] == 0)
					{
						if (stoi(newline.substr(14, 3)) >= MB_MIN_ADDRESS && stoi(newline.substr(14, 3)) <= MB_MAX_ADDRESS)
						{
							mbAddress[bdID - 1] = stoi(newline.substr(14, 3));
							bInvalidAddress = false;
						}
						else if (stoi(newline.substr(14, 3)) == -1)
						{
							isExcludedBD[bdID - 1] = true;
							bInvalidAddress = false;
						}
						else bInvalidAddress = true; //invalid Modbus address
					}
				}
			}
			else if (newline.substr(0, 6) == "Kamera") //camera ID
			{
				if (bdID > 0 && bdID <= CAMERAS_MAX_ADDRESS)
				{
					if (!bInvalidAddress) mbCameraID[bdID - 1] = stoi(newline.substr(8, 2));
				}
				else
				{
					bInvalidAddress = true;
				}
			}
			else if (newline.substr(0, 14) == "Port Szeregowy") //serial port name (e.g. /dev/ttyS0)
			{
				if (bCameraConfigRead && !bInvalidAddress)
				{
					if (newline.substr(16, 5) == "/dev/")
					{
						mbCameraSerialPort = newline.substr(16, 14);
						bCameraConfigOK = true;
						bCameraConfigRead = false;
					}
				}
				else if (bdID > 0 && bdID <= NUM_BD && mbSerialPorts[bdID - 1].empty() && !bInvalidAddress)
				{
					if (newline.substr(16, 5) == "/dev/")
					{
						mbSerialPorts[bdID - 1] = newline.substr(16, 14);
						bdNum++;
					}
					else mbAddress[bdID - 1] = 0; //invalid serial port
				}
			}
		}
	}

	configfile.close();

	if ((bdNum == NUM_BD) && bCameraConfigOK) return true; //successfully read configuration of all diagnostic blocks
	else return false; //failed to read configuration of all diagnostic blocks and/or camera controller
}

bool cModbusGateway::CheckExcludedBD(uint8_t bdID)
{
	lock_guard<mutex> guard_config(mu_config);
	return isExcludedBD[bdID];
}

eMode cModbusGateway::GetUserMode()
{
	lock_guard<mutex> guard_state(mu_state);
	return eUserMode;
}

void cModbusGateway::SetUserMode(eMode eNewUserMode)
{
	if (eUserMode != eNewUserMode)
	{
		{
			lock_guard<mutex> guard_state(mu_state);
			eUserMode = eNewUserMode;
		}

		/********************************** Remote Data ***************************************/

		{
			lock_guard<mutex> guard_remote(mu_remote);
			mb_mapping->tab_registers[2] = (uint16_t) eUserMode;
		}
	}
}

eTract cModbusGateway::GetActiveTract()
{
	lock_guard<mutex> guard_state(mu_state);
	return eActiveTract;
}

void cModbusGateway::SetActiveTract(eTract eNewTract)
{
	if ((int32_t)eNewTract >= 0 && (int32_t)eNewTract <= NUM_TRACTS)
	{
		if (GetActiveTract() != eNewTract)
		{
			StopRTU(); //stop RTU communication

			{
				lock_guard<mutex> guard_state(mu_state);
				eActiveTract = eNewTract;
			}

			//Update active tract in Modbus mapping
			if (GetUserMode() == Local)
			{
				lock_guard<mutex> guard_remote(mu_remote);
				mb_mapping->tab_registers[0] = (int32_t) eNewTract;
			}

			if (GetUserMode() != Waiting)
			{
				//Update tracts window
				NCClearWinTracts();
				NCUpdateWinTracts();

				//Clear all other windows (updated in RTU thread)
				if (GetUserMode() == Local) NCClearWinMotors();
				NCClearWinCameras();
				NCClearWinBD();
			}

			if (GetActiveTract() != None) StartRTU(); //start RTU communication
		}
		else
		{
			NCUpdateWinTracts();
			NCUpdateWinMotors();
		}
	}
}

void cModbusGateway::SetActiveCamera(int32_t bdID)
{
	if (bdID >= 0 && bdID <= NUM_BD)
	{
		bCamerasUpdating = true;

		if (GetUserMode() == Local)
		{
			{
				{
					lock_guard<mutex> guard_state(mu_state);
					if (bdID == bdCurrentCamera) bdNewCamera = 0;
					else bdNewCamera = bdID;
				}

				//Write to camera controller
				{
					lock_guard<mutex> guard_mbcam(mu_mbcam);
					if (bdNewCamera > 0) mbCameraError = modbus_write_register(ctx_camera, 40001, mbCameraID[bdNewCamera - 1]);
					else mbCameraError = modbus_write_register(ctx_camera, 40001, 0);
				}

				/********************************** Local Data ****************************************/

				if (mbCameraError != -1)
				{
					lock_guard<mutex> guard_state(mu_state);
					bdCurrentCamera = bdNewCamera; //update active camera
				}

				{
					lock_guard<mutex> guard_remote(mu_local);
					DiscreteInputs[NUM_BD*NUM_INPUTS_BD + NUM_BD*NUM_ERRORS_LIBMODBUS] = (mbCameraError == -1) ? 1 : 0; //update errors
				}

				/********************************** Remote Data ****************************************/

				{
					lock_guard<mutex> guard_remote(mu_remote);
					if (mbCameraError != -1) mb_mapping->tab_registers[1] = bdNewCamera; //update active camera
					mb_mapping->tab_input_bits[NUM_BD*NUM_INPUTS_BD + NUM_BD*NUM_ERRORS_LIBMODBUS] = (mbCameraError == -1) ? 1 : 0; //update errors
				}

				//Update camera selection window
				NCUpdateWinCameras();
			}
		}
		else if (GetUserMode() == Remote)
		{
			if (bdCurrentCamera != bdID)
			{
				{
					lock_guard<mutex> guard(mu_state);
					bdNewCamera = bdID;
				}

				//Write to camera controller
				{
					lock_guard<mutex> guard_mbcam(mu_mbcam);
					if (bdNewCamera > 0) mbCameraError = modbus_write_register(ctx_camera, 40001, mbCameraID[bdNewCamera - 1]);
					else mbCameraError = modbus_write_register(ctx_camera, 40001, 0);
				}

				//Check RTU errors
				if (mbCameraError != -1) //write successful
				{
					lock_guard<mutex> guard_state(mu_state);
					bdCurrentCamera = bdNewCamera;
				}
				else //write error
				{
					/********************************** Remote Data ***************************************/

					lock_guard<mutex> guard_remote(mu_remote);
					mb_mapping->tab_registers[1] = bdCurrentCamera; //reset remote camera setting
				}

				/********************************** Local Data ****************************************/

				{
					lock_guard<mutex> guard_local(mu_local);
					DiscreteInputs[NUM_BD*NUM_INPUTS_BD + NUM_BD*NUM_ERRORS_LIBMODBUS] = (mbCameraError == -1) ? 1 : 0; //update errors
				}

				/********************************** Remote Data ***************************************/

				{
					lock_guard<mutex> guard_remote(mu_remote);
					mb_mapping->tab_input_bits[NUM_BD*NUM_INPUTS_BD + NUM_BD*NUM_ERRORS_LIBMODBUS] = (mbCameraError == -1) ? 1 : 0; //update errors
				}

				//Update camera selection window
				NCUpdateWinCameras();
			}
		}

		bCamerasUpdating = false;
	}
}

void cModbusGateway::SetMotorCommand(uint8_t bdID)
{
	if (bdID > 0 && bdID <= NUM_BD)
	{
		int32_t mbError;

		if (GetUserMode() == Local)
		{
			{
				lock_guard<mutex> guard_local(mu_local);
				bdMotorStatus = DiscreteInputs[(bdID - 1) * NUM_INPUTS_BD];
			}

			//Write to motor coil
			if (bdMotorStatus)
			{
				lock_guard<mutex> guard_modbus(mu_mbrtu[bdID - 1]);
				mbError = modbus_write_bit(ctx_rtu[bdID - 1], 1, 0);
			}
			else
			{
				lock_guard<mutex> guard_modbus(mu_mbrtu[bdID - 1]);
				mbError = modbus_write_bit(ctx_rtu[bdID - 1], 1, 1);
			}

			/********************************** Local Data ****************************************/

			{
				lock_guard<mutex> guard_local(mu_local);
				DiscreteInputs[NUM_BD * NUM_INPUTS_BD + bdID * NUM_ERRORS_LIBMODBUS + 2] = (mbError == -1) ? 1 : 0; //update errors
			}

			/********************************** Remote Data ***************************************/

			{
				lock_guard<mutex> guard_remote(mu_remote);
				mb_mapping->tab_input_bits[NUM_BD * NUM_INPUTS_BD + bdID * NUM_ERRORS_LIBMODBUS + 2] = (mbError == -1) ? 1 : 0; //update errors
			}
		}
		else if (GetUserMode() == Remote || GetUserMode() == Waiting)
		{
			{
				lock_guard<mutex> guard(mu_remote);
				bdMotorSetting = mb_mapping->tab_bits[bdID - 1];
			}

			{
				lock_guard<mutex> guard_modbus(mu_mbrtu[bdID - 1]);
				mbError = modbus_write_bit(ctx_rtu[bdID - 1], 1, bdMotorSetting); //write to motor coil
			}

			if (mbError != -1) //write successful
			{
				/********************************** Local Data ****************************************/

				lock_guard<mutex> guard_local(mu_local);
				Coils[bdID - 1] = bdMotorSetting;
			}
			else //write error
			{
				/********************************** Remote Data ***************************************/

				lock_guard<mutex> guard(mu_remote);
				mb_mapping->tab_bits[bdID - 1] = Coils[bdID - 1];
			}

			/********************************** Local Data ****************************************/

			{
				lock_guard<mutex> guard_local(mu_local);
				DiscreteInputs[NUM_BD * NUM_INPUTS_BD + bdID * NUM_ERRORS_LIBMODBUS + 2] = (mbError == -1) ? 1 : 0; //update errors
			}

			/********************************** Remote Data ***************************************/

			{
				lock_guard<mutex> guard_remote(mu_remote);
				mb_mapping->tab_input_bits[NUM_BD * NUM_INPUTS_BD + bdID * NUM_ERRORS_LIBMODBUS + 2] = (mbError == -1) ? 1 : 0; //update errors
			}
		}
	}
}

bool cModbusGateway::IsInitializedRTU() const
{
	return bRTUInit;
}

void cModbusGateway::SetInitializeRTU()
{
	lock_guard<mutex> guard(mu_state);
	if (bRTUInit) bRTUInit = false;
}

/* Modbus RTU thread */
void cModbusGateway::ThreadRTU()
{
	uint8_t bdID;
	uint8_t numBD;
	const uint8_t* activeBD;
	int32_t mbErrors[NUM_ERRORS_LIBMODBUS - 1];

    //Select active BD
	activeBD = tract2bd_id[GetActiveTract()];
	numBD = tract2bd_num[GetActiveTract()];

	uint8_t mDiscretes[numBD][NUM_INPUTS_BD] = { 0 };

	//Create Modbus RTU configurations
	for (uint8_t i = 0; i < numBD; i++)
	{
		bdID = activeBD[i] - 1;

		if (!CheckExcludedBD(bdID))
		{
			//Create RTU configuration
			ctx_rtu[bdID] = modbus_new_rtu(mbSerialPorts[bdID].c_str(), MB_BAUD, 'E', 8, 1);
			modbus_set_slave(ctx_rtu[bdID], mbAddress[bdID]);
			modbus_rtu_set_serial_mode(ctx_rtu[bdID], MODBUS_RTU_RS232);
			modbus_set_response_timeout(ctx_rtu[bdID], 0, MB_RESPONSE_TIMEOUT); //TIMEOUT (75ms BD RTU Response)
			modbus_set_byte_timeout(ctx_rtu[bdID], 0, 1000000/MB_15CHAR_TIME); //TIMEOUT (1.5 CHAR TIME)

			//Start connection
			modbus_connect(ctx_rtu[bdID]);
		}
	}

	while (!bTerminateRTU)
	{
		for (uint8_t i = 0; i < numBD; i++)
		{
			bdID = activeBD[i] - 1;

			if (!CheckExcludedBD(bdID))
			{
				lock_guard<mutex> guard_local(mu_local);

				//Clear errors
				mbErrors[0] = 0;
				mbErrors[1] = 0;

				/********************************** Modbus RTU Read ***********************************/

				{
					lock_guard<mutex> guard_modbus(mu_mbrtu[bdID]);
					mbErrors[0] = modbus_read_input_bits(ctx_rtu[bdID], 10001, NUM_INPUTS_BD, &DiscreteInputs[bdID * NUM_INPUTS_BD]); //read motor status and errors
					mbErrors[1] = modbus_read_input_registers(ctx_rtu[bdID], 30001, 1, &InputRegisters[bdID]); //read ADC register
				}

				/********************************** Local Data ****************************************/

				//Store errors as discrete inputs (1 = error, 0 = no error)
				DiscreteInputs[NUM_BD * NUM_INPUTS_BD + bdID * NUM_ERRORS_LIBMODBUS] = (mbErrors[0] == -1) ? 1 : 0;
				DiscreteInputs[NUM_BD * NUM_INPUTS_BD + bdID * NUM_ERRORS_LIBMODBUS + 1] = (mbErrors[1] == -1) ? 1 : 0;

				if (mbErrors[0] == -1 || mbErrors[1] == -1 || DiscreteInputs[NUM_BD * NUM_INPUTS_BD + bdID * NUM_ERRORS_LIBMODBUS + 2])
				{
					if (!bDisconnected[bdID]) //CONNECTED -> DISCONNECTED
					{
						bUpdateMotors = true;
						bDisconnected[bdID] = true;
					}
				}
				else
				{
					if (bDisconnected[bdID]) //DISCONNECTED -> CONNECTED
					{
						//Clear errors
						mbErrors[0] = 0;
						mbErrors[1] = 0;
						DiscreteInputs[NUM_BD * NUM_INPUTS_BD + bdID * NUM_ERRORS_LIBMODBUS + 2] = 0;
						bDisconnected[bdID] = false;
					}

					//Check if ADC sensitivity is high
					if ((InputRegisters[bdID] & 0x8000) != 0) //high sensitivity
					{
						InputRegisters[bdID] &= 0x0FFF;
						InputRegisters[bdID] /= ADC_HSENS_SCALING;
					}

					//Check if motor information has changed
					for (uint8_t j = 0; j < NUM_INPUTS_BD; j++)
					{
						if (mDiscretes[i][j] != DiscreteInputs[bdID * NUM_INPUTS_BD + j]) //new motor information
						{
							if (bRTUInit && !bUpdateMotors) bUpdateMotors = true;
							mDiscretes[i][j] = DiscreteInputs[bdID * NUM_INPUTS_BD + j];
						}
					}
				}

				/********************************** Remote Data ***************************************/

				if (GetUserMode() == Remote || GetUserMode() == Waiting)
				{
					lock_guard<mutex> guard_remote(mu_remote);

					//Update discrete inputs
					uint16_t ibIndex = bdID * NUM_INPUTS_BD;
					for (uint8_t j = 0; j < NUM_INPUTS_BD; j++) mb_mapping->tab_input_bits[ibIndex + j] = DiscreteInputs[ibIndex + j];

					//Update RTU errors
					uint16_t ibErrorIndex = NUM_BD * NUM_INPUTS_BD + bdID * NUM_ERRORS_LIBMODBUS;
					mb_mapping->tab_input_bits[ibErrorIndex] = DiscreteInputs[ibErrorIndex + 0];
					mb_mapping->tab_input_bits[ibErrorIndex + 1] = DiscreteInputs[ibErrorIndex + 1];
					mb_mapping->tab_input_bits[ibErrorIndex + 2] = DiscreteInputs[ibErrorIndex + 2];

					//Update input register
					mb_mapping->tab_input_registers[bdID] = InputRegisters[bdID];
				}

				this_thread::sleep_for(std::chrono::milliseconds(RTU_THREAD_TIMEOUT)); //delay next RTU operations
			}
		}

		//Update main display
		if (GetUserMode() != Waiting) NCUpdateWinBD();

		if (!bRTUInit) //update motor and camera windows when switching tract in local mode
		{
			if (GetUserMode() == Local) NCUpdateWinMotors();
			NCUpdateWinCameras();
			bRTUInit = true;
		}
		else if (bUpdateMotors)
		{
			NCUpdateWinMotors();
			bUpdateMotors = false;
		}

		this_thread::sleep_for(std::chrono::milliseconds(100)); //delay next RTU operations
	}

	//Free resources
	for (uint8_t i = 0; i < numBD; i++)
	{
		bdID = activeBD[i] - 1;

		if (!CheckExcludedBD(bdID))
		{
			modbus_close(ctx_rtu[bdID]);
			modbus_free(ctx_rtu[bdID]);
		}
	}

	bRTUInit = false;
	bTerminateRTU = false;
}

/* Camera thread */
void cModbusGateway::ThreadCameras()
{
	while (!bTerminateCameras)
	{
		if (!bCamerasUpdating)
		{
			/*********************** Check Camera Controller Connection ***************************/

			{
				lock_guard<mutex> guard_mbcam(mu_mbcam);
				mbCameraError = modbus_read_registers(ctx_camera, 40001, 1, &tempRegister);
			}

			if (mbCameraError != -1 && bCamerasConnectionLost)
			{
				//Connection re-established, send camera configuration again
				{
					lock_guard<mutex> guard_mbcam(mu_mbcam);
					mbCameraError = modbus_write_register(ctx_camera, 40001, mbCameraID[bdCurrentCamera - 1]);
				}

				//Update camera selection window, clear connection lost flag
				NCUpdateWinCameras();
				bCamerasConnectionLost = false;
			}
			else if (mbCameraError == -1 && !bCamerasConnectionLost)
			{
				//Update camera selection window, set connection lost flag
				NCUpdateWinCameras();
				bCamerasConnectionLost = true;
			}

			/****************************** Local Data (Camera) ***********************************/

			{
				lock_guard<mutex> guard_local(mu_local);
				DiscreteInputs[NUM_BD*NUM_INPUTS_BD + NUM_BD*NUM_ERRORS_LIBMODBUS] = (mbCameraError == -1) ? 1 : 0; //update errors
			}

			/****************************** Remote Data (Camera) **********************************/

			{
				lock_guard<mutex> guard_remote(mu_remote);
				mb_mapping->tab_registers[1] = tempRegister;
				mb_mapping->tab_input_bits[NUM_BD*NUM_INPUTS_BD + NUM_BD*NUM_ERRORS_LIBMODBUS] = (mbCameraError == -1) ? 1 : 0; //update errors
			}

			this_thread::sleep_for(std::chrono::milliseconds(100)); //100ms delay
		}
	}
}

/* Modbus TCP/IP thread */
void cModbusGateway::ThreadTCP()
{
	uint8_t *tcpQuery = (uint8_t*)malloc(MODBUS_TCP_MAX_ADU_LENGTH);
	modbus_t* ctx_tcp;
    int32_t rc = -1;
    int32_t rp = -1;
    int32_t mapActiveTract = 0;
    int32_t mapActiveCamera = 0;
    int32_t mapMotor = 0;
    uint8_t bdID = 0;
	uint8_t numBD;
	const uint8_t* activeBD;
    bool bSetActiveTract = false;
    bool bSetActiveCamera = false;
    bool bSetMotor = false;

	//Create new TCP/IP configuration
	ctx_tcp = modbus_new_tcp_pi(NULL, "1502");
	socket = modbus_tcp_pi_listen(ctx_tcp, 1);

	while (!bTerminateTCP)
	{
		if (ct == -1) //connection NOK
		{
			ct = modbus_tcp_pi_accept(ctx_tcp, &socket); //wait for new connections (BLOCKING)
		}
		else //connection OK
		{
			rc = modbus_receive(ctx_tcp, tcpQuery);	//wait for new indication (BLOCKING)

			if (rc != -1) //process client request and send reply
			{
				{
					lock_guard<mutex> guard_remote(mu_remote);
					rp = modbus_reply(ctx_tcp, tcpQuery, rc, mb_mapping); //send reply
				}

				if (rp != -1) //no reply errors
				{
					if (GetUserMode() == Remote || GetUserMode() == Waiting) //REMOTE/WAITING MODE
					{
						{
							lock_guard<mutex> guard_remote(mu_remote);

							if (mb_mapping->last_indication.function == MODBUS_FC_WRITE_SINGLE_REGISTER)
							{
								if (mb_mapping->last_indication.start_address == mb_mapping->start_registers)
								{
									bSetActiveTract = true;
									mapActiveTract = mb_mapping->tab_registers[0];
								}
								else if (mb_mapping->last_indication.start_address == mb_mapping->start_registers + 1)
								{
									bSetActiveCamera = true;
									mapActiveCamera = mb_mapping->tab_registers[1];
								}
								else if (mb_mapping->last_indication.start_address == mb_mapping->start_registers + 2)
								{
									//Ignore attempts at Remote setting of user mode
									mb_mapping->tab_registers[2] = (uint16_t) GetUserMode();
								}
							}
							else if (mb_mapping->last_indication.function == MODBUS_FC_WRITE_SINGLE_COIL)
							{
								bSetMotor = true;
								mapMotor = mb_mapping->last_indication.start_address;
							}
							else if (mb_mapping->last_indication.function == MODBUS_FC_WRITE_MULTIPLE_REGISTERS)
							{
								//Reset holding registers to latest settings (writing to multiple registers not allowed)
								mb_mapping->tab_registers[0] = (uint16_t) GetActiveTract();
								mb_mapping->tab_registers[1] = bdCurrentCamera;
								mb_mapping->tab_registers[2] = (uint16_t) GetUserMode();
							}
							else if (mb_mapping->last_indication.function == MODBUS_FC_WRITE_AND_READ_REGISTERS)
							{
								//Reset holding registers to latest settings (writing to multiple registers not allowed)
								mb_mapping->tab_registers[0] = (uint16_t) GetActiveTract();
								mb_mapping->tab_registers[1] = bdCurrentCamera;
								mb_mapping->tab_registers[2] = (uint16_t) GetUserMode();
							}
							else if (mb_mapping->last_indication.function == MODBUS_FC_WRITE_MULTIPLE_COILS)
							{
								//Reset all coils to latest settings (writing to multiple coils not allowed)
								for (uint8_t i = 0; i < NUM_BD; i++) mb_mapping->tab_bits[i] = Coils[i];
							}
						}

						//New tract selected
						if (bSetActiveTract)
						{
							SetActiveTract(static_cast<eTract>(mapActiveTract));
							bSetActiveTract = false;

							if (GetUserMode() == Waiting) //WAITING MODE -> REMOTE MODE (Confirmation Received)
							{
								SetUserMode(Remote);
								NCDeleteWaitingMessage();
								NCClearWinMain();
								NCDrawMainWindows();
								NCDrawBDWindow();
								NCDrawUserControlWindows();
							}
						}

						//New camera selected
						if (bSetActiveCamera)
						{
							if (GetActiveTract() != None)
							{
								//Get active BD
								activeBD = tract2bd_id[GetActiveTract()];
								numBD = tract2bd_num[GetActiveTract()];

								for (uint8_t i = 0; i < numBD; i++)
								{
									bdID = activeBD[i];
									if (bdID == mapActiveCamera) break;
								}

								if (bdID == mapActiveCamera || (mapActiveCamera == 0)) //set new camera
								{
									SetActiveCamera(mapActiveCamera);
								}
								else //reset mapping (ignore command)
								{
									lock_guard<mutex> guard(mu_remote);
									mb_mapping->tab_registers[1] = bdCurrentCamera;
								}
							}
							else //reset mapping (ignore command)
							{
								lock_guard<mutex> guard(mu_remote);
								mb_mapping->tab_registers[1] = bdCurrentCamera;
							}

							bSetActiveCamera = false;
						}

						if (bSetMotor)
						{
							if (GetActiveTract() != None)
							{
								//Get active BD
								activeBD = tract2bd_id[GetActiveTract()];
								numBD = tract2bd_num[GetActiveTract()];

								for (uint8_t i = 0; i < numBD; i++)
								{
									bdID = activeBD[i];
									if (bdID == mapMotor) break;
								}

								if (bdID == mapMotor) //send new motor command
								{
									SetMotorCommand(mapMotor);
								}
								else //reset mapping (ignore command)
								{
									lock_guard<mutex> guard(mu_remote);
									mb_mapping->tab_bits[bdID - 1] = Coils[bdID - 1];
								}
							}
							else //reset mapping (ignore command)
							{
								lock_guard<mutex> guard(mu_remote);
								mb_mapping->tab_bits[bdID - 1] = Coils[bdID - 1];
							}

							bSetMotor = false;
						}
					}
				}
				else if (errno != ENOPROTOOPT) //REPLY ERROR
				{
					RestartSocketTCP(ctx_tcp);
				}
			}
			else if (errno != EMBBADDATA) //RECEIVE ERROR
			{
				RestartSocketTCP(ctx_tcp); //connection closed by client or error
			}
		}
	}

	if (socket != -1) close(socket);
    free(tcpQuery);
	bTerminateTCP = false;
}

void cModbusGateway::RestartSocketTCP(modbus_t* ctx_tcp)
{
	ct = -1;
	if (socket != -1) close(socket);
    socket = modbus_tcp_pi_listen(ctx_tcp, 1); //create new socket
}

void cModbusGateway::StartTCP()
{
	if (!bTCPActive)
	{
		bTCPActive = true;
		threadTCP = thread(&cModbusGateway::ThreadTCP, this);
	}
}

void cModbusGateway::StartRTU()
{
	if (!bRTUActive)
	{
		bRTUActive = true;
		threadRTU = thread(&cModbusGateway::ThreadRTU, this);
	}
}

void cModbusGateway::StopTCP()
{
	if (bTCPActive)
	{
		{
			lock_guard<mutex> guard(mu_thread);
			bTerminateTCP = true;
		}

		threadTCP.detach();
		bTCPActive = false;
	}
}

void cModbusGateway::StopRTU()
{
	if (bRTUActive)
	{
		{
			lock_guard<mutex> guard(mu_thread);
			bTerminateRTU = true;
		}

		threadRTU.join();
		bRTUActive = false;
	}
}

void cModbusGateway::StartCameras()
{
	if (!bCamerasActive)
	{
		bCamerasActive = true;
		threadCameras = thread(&cModbusGateway::ThreadCameras, this);
	}
}

void cModbusGateway::StopCameras()
{
	if (bCamerasActive)
	{
		{
			lock_guard<mutex> guard(mu_thread);
			bTerminateCameras = true;
		}

		threadCameras.join();
		bCamerasActive = false;
	}
}

cModbusGateway::~cModbusGateway()
{
	StopTCP(); //stop TCP/IP thread
	StopRTU(); //stop RTU thread
	StopCameras(); //stop cameras thread
	modbus_close(ctx_camera);
	modbus_free(ctx_camera);
}
