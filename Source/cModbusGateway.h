#ifndef CMODBUSGATEWAY_H_
#define CMODBUSGATEWAY_H_

//Headers
#include <fstream>
#include <fcntl.h>
#include <unistd.h>
#include <cstdint>
#include <cstring>
#include <chrono>
#include <thread>
#include <mutex>
#include <map>
#include "modbus.h"
#include "ncWindows.h"
#include "params.h"

using namespace std;

//Modbus Gateway Class
class cModbusGateway
{
public:
	cModbusGateway(); 	//constructor
	~cModbusGateway();	//destructor

public: //Modbus Gateway API
	bool Init(void);
	bool IsInitializedRTU(void) const;
	bool CheckExcludedBD(uint8_t);
	void SetInitializeRTU(void);
	void SetUserMode(eMode eNewUserMode);
	void SetActiveTract(eTract);
	void SetActiveCamera(int32_t);
	void SetMotorCommand(uint8_t);
	eMode GetUserMode();
	eTract GetActiveTract();

public: //tract-to-BD mappings
    map<eTract, const uint8_t*> tract2bd_id = {{A, bdTractA}, {B, bdTractB}, {C1, bdTractC1},
    		{C2, bdTractC2}, {C3, bdTractC3}, {D, bdTractD}};
    map<eTract, uint8_t> tract2bd_num = {{A, sizeof(bdTractA)}, {B, sizeof(bdTractB)}, {C1, sizeof(bdTractC1)},
    		{C2, sizeof(bdTractC2)}, {C3, sizeof(bdTractC3)}, {D, sizeof(bdTractD)}};

public: //shared local data
    mutex mu_local; //local shared mutex
    mutex mu_state; //shared state changes
    uint8_t Coils[NUM_BD] = { 0 };
    uint8_t DiscreteInputs[NUM_BD*NUM_INPUTS_BD + NUM_ERRORS_LIBMODBUS*NUM_BD + NUM_BD + 1] = { 0 };
    uint16_t InputRegisters[NUM_BD] = { 0 };
    uint16_t bdCurrentCamera;

private:
    modbus_mapping_t* mb_mapping; //gateway mapping
    mutex mu_remote; //remote data mutex
    mutex mu_mbrtu[NUM_BD]; //modbus RTU mutex
    mutex mu_mbcam; //modbus RTU mutex

private:
	bool LoadConfiguration(void);
	void RestartSocketTCP(modbus_t*);

private: //multithreading
	void ThreadRTU(void);
	void ThreadCameras(void);
	void ThreadTCP(void);
	void StartCameras(void);
	void StartRTU(void);
	void StartTCP(void);
	void StopCameras(void);
	void StopRTU(void);
	void StopTCP(void);

private: //multithreading
    thread threadTCP;
    thread threadRTU;
    thread threadCameras;
    mutex mu_thread;  //thread management
    mutex mu_config;  //BD configuration

private: //Modbus data sizes
    uint32_t NumCoils;
    uint32_t NumDiscreteInputs;
    uint32_t NumInputRegisters;
    uint32_t NumHoldingRegisters;

private: //user mode, tract and camera control
    eMode eUserMode;
    eTract eActiveTract;
    int32_t bdNewCamera;
    uint8_t bdMotorSetting;
    uint8_t bdMotorStatus;
    uint8_t mbErrorStatus;
    uint16_t tempRegister;

private: //tract BDs
    const uint8_t bdTractA[4] = {1, 2, 13, 14};
    const uint8_t bdTractB[3] = {1, 2, 15};
    const uint8_t bdTractC1[5] = {1, 2, 3, 4, 9};
    const uint8_t bdTractC2[7] = {1, 2, 3, 4, 5, 6, 7};
    const uint8_t bdTractC3[5] = {1, 2, 3, 4, 8};
    const uint8_t bdTractD[7] = {1, 2, 10, 11, 12, 16, 17};

private: //booleans
    bool bTerminateTCP;
    bool bTerminateRTU;
    bool bTerminateCameras;
    bool bTCPActive;
    bool bRTUActive;
    bool bCamerasActive;
    bool bRTUInit;
    bool bUpdateMotors;
    bool bCamerasUpdating;
    bool bCamerasConnectionLost;
    bool bDisconnected[NUM_BD];
    bool isExcludedBD[NUM_BD];

private: //Modbus RTU
	string mbSerialPorts[NUM_BD];
	string mbCameraSerialPort;
    int32_t mbCameraAddress;
    uint8_t mbCameraID[NUM_BD];
    int32_t mbAddress[NUM_BD] = { 0 };
	int32_t mbCameraError;
	int32_t ct_camera;
	modbus_t* ctx_rtu[NUM_BD];
    modbus_t* ctx_camera;

private: //modbus TCP/IP
    int32_t ct;
    int32_t socket;
};

#endif

