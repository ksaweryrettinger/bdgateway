#ifndef CMODBUSGATEWAYPARAMS_
#define CMODBUSGATEWAYPARAMS_

#include <cstdint>
#include "cModbusGateway.h"

class cModbusGateway;
extern cModbusGateway* mbGateway;

//ADC SETTINGS
const int32_t ADC_HSENS_SCALING = 16;
const float ADC_SCALING = 0.39158;

//MODBUS MAPPING
const int32_t MB_NUM_INPUTREG = 153;

//OTHER CONSTANTS
const uint8_t NUM_BD = 17;
const uint8_t NUM_TRACTS = 6;
const uint8_t NUM_INPUTS_BD = 5;
const uint8_t NUM_ERRORS_LIBMODBUS = 3;
const uint8_t NUM_CAMERAS = 17;
const uint8_t CAMERAS_MAX_ADDRESS = 31;
const uint8_t MB_MIN_ADDRESS = 1;
const uint8_t MB_MAX_ADDRESS = 247;
const uint8_t MB_NUM_INDICATIONS_HISTORY = 100;
const int32_t MB_BAUD = 19200;

//TIMEOUTS
const int32_t MB_15CHAR_TIME = (MB_BAUD/8)*1.5f;
const int32_t MB_RESPONSE_TIMEOUT = 75000;	  //75ms RTU
const int32_t RTU_THREAD_TIMEOUT = 10; 	  //10ms timeout on each RTU thread iteration

//Enums
typedef enum { Local = 0, Remote = 1, Waiting = 2 } eMode;
typedef enum { None = 0, A = 1, B = 2, C1 = 3, C2 = 4, C3 = 5, D = 6 } eTract;

#endif
