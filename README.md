Bloki Diagnostyczne - Modbus Gateway

Installation:

1) Install dependancies:

    Refresh repositories using sudo apt-get update, then install:

    - build-essential
    - libncurses5
    - libncurses5-dev
    - libncursesw5
    - libncursesw5-dev
    - automake
    - autoconf
    - libtool
    - openjdk-11-jdk

2) Install libmodbus (modified)

    - Clone modified version of libmodbus from Bitbucket repository:
    
         git clone https://ksaweryrettinger@bitbucket.org/ksaweryrettinger/libmodbus_modified.git    

    - Compile using:

         autogen.sh
         ./configure
         sudo make install

    - Run ldconfig command in terminal after installing.

3) Install Eclipse CDT

    - Download latest version of Eclipse CDT
    - Unpack files into /usr/: sudo tar -zxvf <filename> -C /usr/
    - Create symbolic link: sudo ln -s /usr/eclipse/eclipse /usr/bin/eclipse
    - Create desktop icon: sudo nano /usr/share/applications/eclipse.desktop

        [Desktop Entry]
        Encoding=UTF-8
        Name=Eclipse IDE
        Comment=Eclipse IDE
        Exec=/usr/bin/eclipse
        Icon=/usr/eclipse/icon.xpm
        Terminal=false
        Type=Application
        Categories=Development;Programming
        StartupNotify=false

4) Import project into Eclipse CDT and create Run Configuration:

    - C/C++ Application: /usr/bin/gnome-terminal
    - Arguments tab -> Program arguments: -e ./BDGateway
    - Arguments tab -> Working directory: deselect 'Use default', set to ${workspace_loc:BDGateway/Debug}
    - Deselect 'Allocate Console'in Common tab.

5) Create Eclipse CDT Debug Configuration:

    - In Debug Configurations... select C/C++ Attach to Application and create defualt configuration.
    - Select all available options under the Debugger tab.
    - Deselect Allocate console in Common tab.
    - Go into the /etc/sysctl.d/10-ptrace.conf system file, and set kernel.yama.ptrace_scope = 0.
    - To Debug, launch BDGateway from the terminal, then run the Debug (Attach to Process) configuration.
    - Search for BDGateway in running processes and attach the debugger.

6) Make sure the Linux user is added to the 'dialout' system group, to ensure the application
   has access to /dev/tty* files:

    - sudo usermod -a -G dialout <username>
    - Search for active tty serial ports using: dmesg | grep tty

